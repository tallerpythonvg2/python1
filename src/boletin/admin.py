from django.contrib import admin
from boletin.models import *
from .forms import RegModelForm


class AdminRegistrado(admin.ModelAdmin):
    list_display = ["nombre", "email", "fecha"]
    form = RegModelForm
    list_filter = ["fecha"]
    #list_display_links = ["email"]
    #list_editable = ["nombre"]
    list_editable = ["email"]
    search_fields = ["nombre", "email"]

    #class Meta:
     #   model = Registrado

# Register your models here.


admin.site.register(Registrado, AdminRegistrado)
